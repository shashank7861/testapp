<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('/');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/addproducts', 'MainController@addproducts')->name('addproducts');
Route::get('/addorder',[
	'uses'=>'MainController@add_order',
	'middleware'=>'auth'
]);
Route::get('/reports', [
	'uses'=>'MainController@reports',
	'as'=>'reports',
	'middleware'=>'auth'
]);
Route::get('/get-order-chart-data',[
	'uses'=>'ChartDataController@getDailyOrders',
	'middleware'=>'auth',
	'as'=>'get-order-chart-data'
]);
Route::get('/get-sales-chart-data', [
	'uses'=>'ChartDataController@getProductSales',
	'middleware'=>'auth',
	'as'=>'get-sales-chart-data'
]);
Route::get('/get_price', [
	'uses'=>'MainController@ProductPrice',
	'middleware'=>'auth',
	'as'=>'get_price'
]);
Route::post('/create_order', [
	'uses'=>'MainController@create_order',
	'middleware'=>'auth',
	'as'=>'add.order'
]);
Route::post('/create_product', [
	'uses'=>'MainController@create_product',
	'as'=>'add.product'
]);
