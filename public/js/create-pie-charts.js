
( function ( $ ) {
	var charts = {
		
		init: function () {
			// -- Set new default font family and font color to mimic Bootstrap's default styling
			Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
			Chart.defaults.global.defaultFontColor = '#292b2c';
			//charts.createCompletedJobsChart();
			this.ajaxGetPostDailyData();

		},
		ajaxGetPostDailyData: function () {
			var urlPath = 'get-sales-chart-data';
			var request = $.ajax( {
				method: 'GET',
				url: urlPath
		} );
			request.done( function ( response ) {
			console.log( response );
			charts.createCompletedJobsChart( response );
			});
		},

		/**
		 * Created the Completed Jobs Chart
		 */
		createCompletedJobsChart: function ( response ) {

			var ctx = document.getElementById("myPieChart");
			var myLineChart = new Chart(ctx, {
				type: 'pie',
				data: {
					labels: response.product_name, // The response got from the ajax request containing all month names in the database
					datasets: [{
						lineTension: 0.3,
						backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#28a745'],
						borderColor: "rgba(2,117,216,1)",
						pointRadius: 5,
						pointBackgroundColor: "rgba(2,117,216,1)",
						pointBorderColor: "rgba(255,255,255,0.8)",
						pointHoverRadius: 5,
						pointHoverBackgroundColor: "rgba(2,117,216,1)",
						pointHitRadius: 20,
						pointBorderWidth: 1,
						data: response.product_qty // The response got from the ajax request containing data for the completed jobs in the corresponding months
					}],
				},
			});
		}
	};

	charts.init();

} )( jQuery );