@extends('layouts.app')

@section('content')
<div class="container">
    <h1 style="text-align: center;">ABC Restaurant</h1>
    <div class="row">
        <div class="col-lg-24">
            <a href="{{ url('/addorder') }}"><button class="btn btn-success btn-lg">Add Order</button></a>
            <a href="{{ url('/reports') }}"><button style="float: right;" class="btn btn-warning btn-lg">View Report</button></a>
        </div>
    </div>
    <!-- Area Chart Example-->
    <div class="col-lg-6">
        <div class="card-header">
        	<h3 style="text-align: center;">Daily Order Data</h3>
        </div>
        <div class="card-body">
            <canvas id="myAreaChart" width="100%" height="30"></canvas>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at @php  echo date('F j, Y', time() ) @endphp</div>
    </div>
    <div class="col-lg-6">
        <div class="card-header">
        	<h3 style="text-align: center;">Product Sale by Quantity</h3>
        </div>
        <div class="card-body">
            <canvas id="myPieChart" width="100%" height="30"></canvas>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at @php  echo date('F j, Y', time() ) @endphp</div>
    </div>
    <div class="col-lg-6">
        <div class="card-header">
        	<h3 style="text-align: center;">Product wise sales</h3>
        </div>
        <div class="card-body">
            <canvas id="myBarChart" width="100%" height="30"></canvas>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at @php  echo date('F j, Y', time() ) @endphp</div>
    </div>


</div>
@endsection

@section( 'scripts' )
    <script src="{{url( 'js/jquery.min.js' )}}"></script>
    <script src="{{url( 'js/chart.min.js' )}}"></script>
    <script src="{{url( 'js/create-line-charts.js' )}}"></script>
    <script src="{{url( 'js/create-bar-charts.js' )}}"></script>
    <script src="{{url( 'js/create-pie-charts.js' )}}"></script>
@endsection
