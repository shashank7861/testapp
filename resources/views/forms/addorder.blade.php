@extends('layouts.app')

@section('content')
<div class="container">
    <h1 style="text-align: center;">ABC Restaurant</h1>
    <div class="row">
        <div class="col-lg-24">
            <a href="{{ url('/addorder') }}"><button class="btn btn-success btn-lg">Add Order</button></a>
            <a href="{{ url('/reports') }}"><button style="float: right;" class="btn btn-warning btn-lg">View Report</button></a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-24">
        	<h2>Add Order</h2>
        	    <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.order') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Product/Item</label>
                            <div class="col-md-6">
                                <select name="product" id="product">
                                    <option value="">Select</option>
                                @foreach($products as $product)
                                	<option value="{{ $product->id}}">{{ $product->name}}</option>
                                @endforeach		
                                </select>
                                @if ($errors->has('product'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="Price" class="col-md-4 control-label">Price</label>
                            <div class="col-md-6">
                                <input id="price" type="text" readonly class="form-control" name="price" required>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Quantity</label>
                            <div class="col-md-6">
                                <input id="qty" type="number" class="form-control" name="qty" required>

                                @if ($errors->has('qty'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="Price" class="col-md-4 control-label">Total Amount</label>
                            <div class="col-md-6">
                                <input id="amount" readonly type="text" class="form-control" name="amount" required>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>    
        </div>
    </div>
</div>
@endsection

@section( 'scripts' )
    <script src="{{url( 'js/jquery.min.js' )}}"></script>
    <script src="{{url( 'js/functions.js' )}}"></script>
@endsection