@extends('layouts.app')

@section('content')
<div class="container">
    <h1 style="text-align: center;">ABC Restaurant</h1>
    <div class="row">
        <div class="col-lg-24">
            <a href="{{ url('/addorder') }}"><button class="btn btn-success btn-lg">Add Order</button></a>
            <a href="{{ url('/reports') }}"><button style="float: right;" class="btn btn-warning btn-lg">View Report</button></a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-24">
        	<h2>Add Order</h2>
        	    <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.product') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="Price" class="col-md-4 control-label">Product</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Cost Per Unit</label>
                            <div class="col-md-6">
                                <input id="cost" type="number" class="form-control" name="cost" required>

                                @if ($errors->has('cost'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cost') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            <hr>
            <h2>Recent Products</h2>
        @if(($products))
        <table id="recent_orders" class="table table-bordered">
            <tr>
                <th>Product/Item</th>
                <th>Unit Price</th>
            </tr>   
            @foreach ($products as $product)
            <tr id="product->id">
                <td>{{$product->name}}</td>
                <td>{{$product->cost}}</td>
            </tr>
            @endforeach
        </table>        
        @endif    
        </div>
    </div>
</div>
@endsection

@section( 'scripts' )
    <script src="{{url( 'js/jquery.min.js' )}}"></script>
    <script src="{{url( 'js/functions.js' )}}"></script>
@endsection