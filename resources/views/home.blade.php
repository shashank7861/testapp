@extends('layouts.app')

@section('content')
<div class="container">
    <h1 style="text-align: center;">ABC Restaurant</h1>
    <div class="row">
        <div class="col-lg-24">
            <a href="{{ url('/addorder') }}"><button class="btn btn-success btn-lg">Add Order</button></a>
            <a href="{{ url('/reports') }}"><button style="float: right;" class="btn btn-warning btn-lg">View Report</button></a>
        </div>
        <h2>Recent Orders</h2>
        <table id="recent_orders" class="table table-bordered">
        	<tr>
        		<th>Product/Item</th>
        		<th>Unit Price</th>
        		<th>Quantity</th>
        		<th>Total Amount</th>
        	</tr>	
        	@foreach ($orders as $order)
        	<tr id="order->id">
        		<td>@foreach ($products as $product)
        				@if($product->id==$order->product_id)
        					{{ $product->name }}
        				@endif	
        			@endforeach
        		</td>
        		<td>@foreach ($products as $product)
        				@if($product->id==$order->product_id)
        					{{ $product->cost }}
        				@endif	
        			@endforeach
        		</td>
        		<td>{{$order->qty}}</td>
        		<td>{{$order->amount}}</td>
        	</tr>
        	@endforeach
        </table>	
    </div>
</div>
@endsection
@section( 'scripts' )
@endsection
