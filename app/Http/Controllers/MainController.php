<?php

namespace OrderManagement\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use OrderManagement\Product;
use OrderManagement\Order;

class MainController extends Controller
{
    public function addproducts()
    {
        $products=Product::All();
        return view('forms.addproduct')->with('products',$products);
        
    }
    public function create_product( Request $request )
    {   
        $this->validate($request,[
            'name' => 'required|string|max:255|unique:products',
            'cost' => 'required|regex:/^\d*(\.\d{1,2})?$/'
        ]);        
        $product=new Product();
        $product->name=$request['name'];
        $product->cost=$request['cost'];
        $product->save();
        $products=Product::All();
        return redirect('/addproducts');
    }
    public function add_order()
    {
    	$products=Product::All();
        return view('forms.addorder')->with('products',$products);
    }
    public function reports()
    {
    	$orders=Order::orderBy('created_at','asce')->get();
        $products=Product::orderBy('created_at','asce')->get();
        //return view('admin.dashboard',['posts'=>$posts,'user'=>Auth::user()]);
        return view('forms.reports',['products'=>$products,'orders'=>$orders]);
    }
    
    public function ProductPrice( Request $request )
    {
        $product=Product::where('id',$request->id)->first();
        if(!empty($product)){
            return $product->cost;   
        }
    }

    public function create_order(Request $request)
    {   
        $this->validate($request,[
            'qty' => 'required|integer|min:1',
            'amount' => 'regex:/^\d*(\.\d{1,2})?$/'
        ]);
        $order=new Order();
    	$order->user_id=Auth::id();
    	$order->product_id=$request['product'];
    	$order->qty=$request['qty'];
		$order->amount=$request['amount'];
		$order->save();
		$product_id=$request['product'];
        $product = Product::where('id',$product_id)->first();
        $sold_qty=$request['qty']+$product->sold_qty;
        $product->sold_qty=$sold_qty;
        $product->update();    
		return redirect('/home')->with('success','Order Created');

    }
    
}
