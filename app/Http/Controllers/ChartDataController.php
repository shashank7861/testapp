<?php

namespace OrderManagement\Http\Controllers;

use Illuminate\Http\Request;
use OrderManagement\Order;
use OrderManagement\Product;
use Auth;
class ChartDataController extends Controller
{
	function getAllDays(){
		$dates_array=array();
		$order_dates= Order::where('user_id',Auth::user()->id)->OrderBy('created_at','ASC')->pluck('created_at');
		$order_dates=json_decode($order_dates);
		if(!empty($order_dates)){
			foreach ($order_dates as $rawdate) {
				$date = new \DateTime( $rawdate->date );
				$date_day = $date->format( 'd/M' );
				$date_count = $date->format( 'd' );
				$dates_array[$date_count] =$date_day;
			}
		}
		else{
			$dates_array='';
		}
		return $dates_array;
		//return $this->getDailyOrderCount(12);
			
	}
	function getDailyOrderCount($date){
		$get_daily_orders_count=Order::where('user_id',Auth::user()->id)->WhereDay('created_at',$date)->get()->count();
		return $get_daily_orders_count;
	}
    function getDailyOrders(){

    	
    	$daily_orders_count=array();
    	$dates_array=$this->getAllDays();
    	$show_dates_array=array();
    	if(!empty($dates_array))
    	{
    		foreach ($dates_array as $date_count => $date_day) {
    			$daily_order_count=$this->getDailyOrderCount($date_day);
    			array_push($daily_orders_count, $daily_order_count);
    			array_push($show_dates_array, $date_day);
    		}
    	}
    	$max_orders=max($daily_orders_count);
    	$max=round(($max_orders+10/2)/10)*10;
    	$datewise_orders_count=array(
    		'dates'=> $show_dates_array,
    		'daily_order_data'=>$daily_orders_count,
    		'max'=>$max,
    	);
    	return $datewise_orders_count;
    }
    function getProductSales(){
    	$product_orders=array();
    	$product_qty=array();
    	$products=Product::All();
    	$maxValue = Product::max('sold_qty');
    	$max=round(($maxValue+10/2)/10)*10;
    	if(!empty($products)){
    		foreach ($products as $product) {
	    		array_push($product_orders,$product->name);
	    		array_push($product_qty,$product->sold_qty);
	    		$product_count=array(
	    			'product_name' => $product_orders,
	    			'product_qty'=>$product_qty,
	    			'max'=>$max,
	    		);
			}
		}
    	return $product_count;
    }

}





