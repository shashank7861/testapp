<?php

namespace OrderManagement\Http\Controllers;

use Illuminate\Http\Request;
use OrderManagement\Product;
use OrderManagement\Order;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders=Order::where('user_id',Auth::user()->id)->orderBy('created_at','asce')->get();
        $product=Product::orderBy('created_at','asce')->get();
        return view('home',['orders'=>$orders,'products'=>$product]);            
        //return view('forms.reports',['products'=>$products,'orders'=>$orders]);    
    }
}
